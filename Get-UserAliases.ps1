# Get-UserAliases.ps1
# Gets a full list of primary emails and their aliases from office 365
#
# Author:               Aaron Coach

#Output filename
$OutputFile = "UserAliases.csv"

#Get credentials
$LiveCred = Get-Credential

#Connect to MSOL Service
connect-msolservice -credential $LiveCred
 
#Prepare Output file with titles 
Out-File -FilePath $OutputFile'.temp' -InputObject "Primary,Aliases" -Encoding UTF8 

#Get all msolusers
$users = get-msoluser -all

foreach ($user in $users) {
    #Use "SMTP" and "smtp" to match "Primary" and "Aliases" respectively and enter into csv file (removing aliases containing communitytraining)
    Out-File -FilePath $OutputFile'.temp' -InputObject "$($user.ProxyAddresses -cmatch 'SMTP'),$($($user.ProxyAddresses -cmatch 'smtp') -replace '\S*@communitytraining\S*', '')" -Encoding UTF8 -append
}

#Sort by primary address
import-csv $OutputFile'.temp' | sort "Primary" -Unique | export-csv -Encoding UTF8 $OutputFile

#Remove temporary output file
rm $OutputFile'.temp'
